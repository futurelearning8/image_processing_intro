from os import path
import cv2
import numpy as np
import numpy.ma as ma

data_path = "../data/videos/"


class BokehEffect:
    def __init__(self):
        self.to_blur = False
        self.com_id = 0

    def left_click_event(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # Threshold it so it becomes binary
            img = cv2.cvtColor(self.masked_image, cv2.COLOR_BGR2GRAY)
            nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img, 8, cv2.CV_32S)
            self.to_blur = True
            self.com_id = output[y, x]

    @ staticmethod
    def initialization(frames, N):
        num_frames = frames.shape[0]
        if N < 1 or N > num_frames:
            print(f"N should be in the range of 1 to {num_frames}")
            N = 5
        median_frames = frames[0:N]
        median = np.median(median_frames, axis=0).astype("uint8")
        return median

    def running(self, frame, ref_frame, th, center, max_size):
        # Convert BGR to HSV
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        ref_frame_hsv = cv2.cvtColor(ref_frame, cv2.COLOR_BGR2HSV)
        # Compute the difference (diff_frame) between the median frame and current frame (HSV domain).
        diff_frame_hsv = cv2.absdiff(frame_hsv, ref_frame_hsv)

        # compare to threshold
        h_s_diff = np.sqrt(np.square(diff_frame_hsv[:, :, 0]) + np.square(diff_frame_hsv[:, :, 1]))
        mask = ma.masked_less_equal(h_s_diff, th).mask

        # Set all masked pixels to zero
        self.masked_image = frame.copy()
        self.masked_image[mask] = 0
        self.masked_image[~mask] = 255

        # reduce holes in background and foreground
        kernel = np.ones((5, 5), np.uint8)
        self.masked_image = cv2.morphologyEx(self.masked_image, cv2.MORPH_CLOSE, kernel)
        kernel = np.ones((3, 3), np.uint8)
        self.masked_image = cv2.erode(self.masked_image, kernel, iterations=2)
        kernel = np.ones((7, 7), np.uint8)
        self.masked_image = cv2.morphologyEx(self.masked_image, cv2.MORPH_OPEN, kernel)

        # cv2.imshow('Frame', cv2.resize(self.masked_image, (960, 540)))
        # cv2.waitKey(25)

        # Blur according chose
        out_frame = frame.copy()
        if self.to_blur:
            if self.com_id:
                blurred_img = cv2.GaussianBlur(frame, (21, 21), 0)
                out_frame = np.where(self.masked_image == np.array([255, 255, 255]), frame, blurred_img)
            else:
                out_frame = cv2.GaussianBlur(frame, (21, 21), 0)

        return out_frame

    def run(self, path_to_video, N, th, center, max_size):
        # self.show_vid(path_to_video)
        frames = self.read_frames(path_to_video)
        ref_frame = self.initialization(frames, N)

        # load the image, clone it, and setup the mouse callback function
        vid = cv2.VideoCapture(path_to_video)
        cv2.namedWindow("image")
        cv2.setMouseCallback("image", self.left_click_event)

        if not vid.isOpened():
            print("Error opening video stream or file!")
        while vid.isOpened():
            ret, frame = vid.read()
            if ret:
                bokeh_frame = self.running(frame, ref_frame, th, center, max_size)
                cv2.imshow('image', bokeh_frame)
                if cv2.waitKey(10) & 0xFF == ord('q'):
                    break
            else:
                break
        cv2.destroyAllWindows()

    @staticmethod
    def read_frames(path_to_video):
        vid = cv2.VideoCapture(path_to_video)
        if not vid.isOpened():
            print("Error opening video stream or file!")
        frames = []
        while vid.isOpened():
            ret, frame = vid.read()
            if ret:
                frames.append(frame)
            else:
                break
        return np.array(frames)

    @staticmethod
    def show_vid(path_to_video):
        # Create a VideoCapture object and read from input file
        vid = cv2.VideoCapture(path_to_video)

        if not vid.isOpened():
            print("Error opening video stream or file!")

        while vid.isOpened():
            ret, frame = vid.read()
            if ret:
                cv2.imshow('Frame', frame)
                if cv2.waitKey(15) & 0xFF == ord('q'):
                    break
            else:
                break

        vid.release()
        cv2.destroyAllWindows()


def check_path(file_path):
    if not path.exists(file_path):
        print("No such file!")
        exit()


def main():
    videos = ["car_with_balloons.mp4", "dorian.mp4", "woman_on_floor.mp4"]
    # Number of frames for median, video Thresholds
    params = [(10, 13), (5, 13), (100, 6)]
    for video_name, (N, th) in zip(videos, params):
        path_to_video = data_path+video_name
        check_path(path_to_video)

        center, max_size = 0, 0

        be = BokehEffect()
        be.run(path_to_video, N, th, center, max_size)


if __name__ == "__main__":
    main()
